from keras.models import Model
from keras.layers import Dense, GlobalAveragePooling2D,Input
from keras.applications.vgg16 import VGG16
from keras.preprocessing.image import ImageDataGenerator
from keras.optimizers import SGD
from keras.callbacks import CSVLogger

import tensorflow as tf

from PIL import ImageFile
ImageFile.LOAD_TRUNCATED_IMAGES = True

from keras.utils import multi_gpu_model
from keras.backend.tensorflow_backend import set_session
config = tf.ConfigProto(
    gpu_options=tf.GPUOptions(
        visible_device_list="4", # specify GPU number
        allow_growth=True
    )
)
set_session(tf.Session(config=config))

import argparse

parser = argparse.ArgumentParser()


parser.add_argument('--dataset_name', default= 'size-512__histgram_equ-False__valid_area_ratio-1__horizon_division_num-13', help='The name of the dataset directory')
parser.add_argument('--train_dir', default='/data/tmc_processed/', help='The path to the dataset')
parser.add_argument('--num_cluster', type=int, default='15', help='The path to save the clustered images')
parser.add_argument('--batch_size', type=int, default='32', help='The path to save the clustered images')

args = parser.parse_args()

n_categories = args.num_cluster
train_dir = args.train_dir 
file_name = args.dataset_name
batch_size = args.batch_size

base_model=VGG16(weights='imagenet',include_top=False,
                 input_tensor=Input(shape=(224,224,3)))

#add new layers instead of FC networks
x=base_model.output
x=GlobalAveragePooling2D()(x)
x=Dense(1024,activation='relu')(x)
prediction=Dense(n_categories,activation='softmax')(x)
model=Model(inputs=base_model.input,outputs=prediction)

#fix weights before VGG16 14layers
for layer in base_model.layers[:15]:
    layer.trainable=False

model.compile(optimizer=SGD(lr=0.0001,momentum=0.9),
              loss='categorical_crossentropy',
              metrics=['accuracy'])

model.summary()

#save model
json_string=model.to_json()
open(file_name+'.json','w').write(json_string)

train_datagen=ImageDataGenerator(
    rescale=1.0/255,
    shear_range=0.2,
    zoom_range=0.2,
    horizontal_flip=True)

#validation_datagen=ImageDataGenerator(rescale=1.0/255)

train_generator=train_datagen.flow_from_directory(
    train_dir + file_name + '/image' ,
    target_size=(224,224),
    batch_size=batch_size,
    class_mode='categorical',
    shuffle=True
)

#validation_generator=validation_datagen.flow_from_directory(
#    validation_dir,
#    target_size=(224,224),
#    batch_size=batch_size,
#    class_mode='categorical',
#    shuffle=True
#)

hist=model.fit_generator(train_generator,
                         epochs=200,
                         verbose=1,
                         steps_per_epoch=200,
                         callbacks=[CSVLogger(file_name+'.csv')])

#save weights
model.save(file_name+'.h5')


