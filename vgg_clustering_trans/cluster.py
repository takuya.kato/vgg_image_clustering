from keras.models import model_from_json
import matplotlib.pyplot as plt
import numpy as np
import os,random
from keras.preprocessing.image import img_to_array, load_img
from keras.preprocessing.image import ImageDataGenerator
from keras.optimizers import SGD
import pandas as pd

batch_size=32
image_num=190
file_name='size-512__histgram_equ-False__valid_area_ratio-1__horizon_division_num-13'
test_dir='size-512__histgram_equ-False__horizon_division_num-13_train/validation'
display_dir='size-512__histgram_equ-False__horizon_division_num-13_train/test'
label=['0','1','2','3','4','5','6','7','8','9','10','11','12','13','14']

#load model and weights
json_string=open(file_name+'.json').read()
model=model_from_json(json_string)
model.load_weights(file_name+'.h5')

model.compile(optimizer=SGD(lr=0.0001,momentum=0.9),
              loss='categorical_crossentropy',
              metrics=['accuracy'])


#data generate
test_datagen=ImageDataGenerator(rescale=1.0/255)

test_generator=test_datagen.flow_from_directory(
    test_dir,
    target_size=(224,224),
    batch_size=batch_size,
    class_mode='categorical',
    shuffle=True
)


#predict model and display images
files=os.listdir(display_dir)
img=random.sample(files,image_num)

predict_label = []

for i in range(image_num):
    temp_img=load_img(os.path.join(display_dir,img[i]),target_size=(224,224))
    temp_img_array=img_to_array(temp_img)
    temp_img_array=temp_img_array.astype('float32')/255.0
    temp_img_array=temp_img_array.reshape((1,224,224,3))
    #predict image
    img_pred=model.predict(temp_img_array)
    predict_label.append([os.path.join(display_dir,img[i]), label[np.argmax(img_pred)]])
    #eliminate xticks,yticks

predict_label_pd = pd.Series(predict_label)

predict_label_pd.to_csv(file_name + "_predicted_label.csv", index=False)
