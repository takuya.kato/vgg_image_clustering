from keras.applications.vgg16 import VGG16, preprocess_input, decode_predictions
from keras.layers import Dense, Dropout, Activation, Flatten
from keras.preprocessing import image
from keras.preprocessing.image import load_img,img_to_array

from sklearn.cluster import KMeans, MiniBatchKMeans
import numpy as np
import pandas as pd
import sys
import cv2
import os
from progressbar import ProgressBar 
import shutil
import glob

import tensorflow as tf
from keras.backend.tensorflow_backend import set_session


import argparse

parser = argparse.ArgumentParser()

parser.add_argument('--dataset_name', default= 'size-512__histgram_equ-False__horizon_division_num-13', help='The name of the dataset directory')
parser.add_argument('--data_dir', default='/data/tmc_190401/proceeded/', help='The path to the dataset')
parser.add_argument('--save_dir', default='/data/tmc_processed/', help='The path to save the clustered images')
parser.add_argument('--mode', default='train', help='The path to save the clustered images')


args = parser.parse_args()

print(args)

DATASET_NAME = args.dataset_name
DATA_DIR = args.data_dir
SAVE_DIR = args.save_dir
MODE = args.mode
IMAGE_LABEL_FILE ='image_label.csv'

from keras.utils import multi_gpu_model
from keras.backend.tensorflow_backend import set_session
config = tf.ConfigProto(
    gpu_options=tf.GPUOptions(
        visible_device_list="4,5", # specify GPU number
        allow_growth=True
    )
)
set_session(tf.Session(config=config))


class Image_Clustering:
	def __init__(self, n_clusters=15, image_file_temp='img_%s.png', input_video=True):
		self.n_clusters = n_clusters            # The number of cluster
		self.image_file_temp = image_file_temp  # Image file name template


	def main(self):
		self.label_images()
		self.classify_images()
		
	def label_images(self):
		print('Label images...')

		# Load a model
		model = VGG16(weights='imagenet', include_top=False)

		#File type
		file_type  = 'png'

		#load images and image to array
		img_list = glob.glob(DATA_DIR + DATASET_NAME + '/' + MODE + '/image/' + '*.' + file_type)

		images = []
		for img in img_list:
			images.append(img)

		images = np.array(images)
		assert(len(images)>0)
		
		X = []
		pb = ProgressBar(max_value=len(images))
		for i in range(len(images)):
			# Extract image features
			feat = self.__feature_extraction(model, images[i])
			X.append(feat)
			pb.update(i)  # Update progressbar

		# Clutering images by k-means++
		X = np.array(X)
		kmeans = MiniBatchKMeans(n_clusters=self.n_clusters, random_state=0).fit(X)
		print('')
		print('labels:')
		print(kmeans.labels_)
		print('')
	
		if not os.path.exists(SAVE_DIR + DATASET_NAME):
                	os.makedirs(SAVE_DIR + DATASET_NAME)
	
		# Merge images and labels
		df = pd.DataFrame({'image': images, 'label': kmeans.labels_})
		df.to_csv(SAVE_DIR + DATASET_NAME + '/' + IMAGE_LABEL_FILE, index=False)


	def __feature_extraction(self, model, img_path):
		img = image.load_img(img_path, target_size=(224, 224))
		x = image.img_to_array(img)
		x = np.expand_dims(x, axis=0)
		x = preprocess_input(x) 

		feat = model.predict(x) 
		feat = feat.flatten() 

		return feat


	def classify_images(self):
		print('Classify images...')
		df = pd.read_csv(SAVE_DIR + DATASET_NAME + '/' + IMAGE_LABEL_FILE)
		labels = list(set(df['label'].values))

		if MODE == 'test':
                	anno_dir = SAVE_DIR + DATASET_NAME + '/annotation/'
                	if not os.path.exists(anno_dir):
                		os.makedirs(anno_dir)

		image_dir = SAVE_DIR + DATASET_NAME + '/image/'
		if not os.path.exists(image_dir):
                	os.makedirs(image_dir)

		for label in labels:
			print('Copy and paste label %s images.' % label)
			
			if MODE == 'test':
				new_dir = SAVE_DIR + DATASET_NAME + '/annotation/' + str(label) + '/'
				if not os.path.exists(new_dir):
					os.makedirs(new_dir)

			new_dir = SAVE_DIR + DATASET_NAME + '/image/' + str(label) + '/'
			if not os.path.exists(new_dir):
				os.makedirs(new_dir)

			clustered_images = df[df['label']==label]['image'].values
			for ci in clustered_images:
				if MODE == 'test':
					src = DATA_DIR + DATASET_NAME + '/' + MODE + '/annotation/' + os.path.basename(ci)
					dst = SAVE_DIR + DATASET_NAME + '/annotation/' + str(label) + '/' + os.path.basename(ci)
					shutil.copyfile(src, dst)

				src = DATA_DIR + DATASET_NAME + '/' + MODE + '/image/' + os.path.basename(ci)
				dst = SAVE_DIR + DATASET_NAME + '/image/' + str(label) + '/' + os.path.basename(ci)
				shutil.copyfile(src, dst)
		
if __name__ == "__main__":
	Image_Clustering().main()
