python image_clustering.py --dataset_name size-512__histgram_equ-False__horizon_division_num-39__train_dilate_value-20 --mode train
python image_clustering.py --dataset_name size-512__histgram_equ-False__horizon_division_num-39__train_dilate_value-25 --mode train
python image_clustering.py --dataset_name size-512__histgram_equ-False__valid_area_ratio-1.0__horizon_division_num-39__train_dilate_value-20 --mode train
python image_clustering.py --dataset_name size-512__histgram_equ-False__valid_area_ratio-1.0__horizon_division_num-39__train_dilate_value-25 --mode train
python image_clustering.py --dataset_name size-512__histgram_equ-False__valid_area_ratio-1__horizon_division_num-13 --mode train
